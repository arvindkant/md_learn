# Boom ⭐
![](https://camo.githubusercontent.com/f70ca4c3e8abab12b88528661ba3f6b0ec26952e/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c6963656e73652d4d49542532304c6963656e73652d626c75652e737667)
![](https://camo.githubusercontent.com/4988e9031e055192f0bf949cbb25ce34d58177c2/68747470733a2f2f6170692e636f646163792e636f6d2f70726f6a6563742f62616467652f47726164652f6664636630323261336666633466386262363433663036613635376337363034)
![](https://camo.githubusercontent.com/55b9a19ff279154b8859dc8306539aa655413ee9/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4150492d31362532422d627269676874677265656e2e7376673f7374796c653d666c6174)

A lightweight library written in Kotlin to implement elastic touch effect in your project.

![](https://user-images.githubusercontent.com/16231428/58156715-06e2f380-7c77-11e9-8c33-e51b93c4cb6f.gif)

## Including in your project
Add `jitpack` maven in your root build.gradle at the end of repositories:
```gradle
allprojects {
  repositories {
   ...
   maven { url 'https://jitpack.io' }
  }
 }
```
Add the dependency to your app `build.gradle` file (not your project build.gradle file).
```gradle
dependencies {
   implementation 'com.github.astrit-veliu:Boom:1.0'
}
```

### Usage
`Boom` is really easy to use and gives to your Views a beautiful touch effect.

## Kotlin
Some of material widgets needs to be casted to View, otherwise it will show `Type mismatch` warning.

```kotlin
  Boom(txt_header)
  Boom(material_text_button as View)
```
## Java
After views are binded simply pass the name of widget to Boom constructor.

```java
  new Boom(txt_header);
  new Boom(material_text_button);
```

## Supports ❤
Feel free to give your support by contributing to this library. <br><br>

# License
```xml
MIT License

Copyright (c) 2019 Astrit Veliu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```